from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField
from wtforms.validators import DataRequired, Length


class NewOwnerForm(FlaskForm):
    username = StringField('username', render_kw={"placeholder": "Enter username"}, validators=[DataRequired(), Length(min=1, max=50)])
    email = StringField('email', render_kw={"placeholder": "Enter email"}, validators=[DataRequired(), Length(min=1, max=50)])
    password = PasswordField('password', render_kw={"placeholder": "Enter password"}, validators=[DataRequired(), Length(min=1, max=50)])
    confirm_password = PasswordField('confirm password', render_kw={"placeholder": "Retype password"}, validators=[DataRequired(), Length(min=1, max=50)])
    owner_name = StringField('owner name', render_kw={"placeholder": "Enter owner full name"}, validators=[DataRequired(), Length(min=1, max=50)])
    occupation = StringField('occupation', render_kw={"placeholder": "Enter occupation"}, validators=[DataRequired(), Length(min=1, max=50)])
    submit = SubmitField('New Owner')

    def check_password(self):
        if self.password.data != self.confirm_password.data:
            return False
        
        return True