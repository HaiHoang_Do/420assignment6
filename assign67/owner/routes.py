from flask import Blueprint, render_template, flash, redirect, url_for
from flask_bcrypt import Bcrypt

from assign67.owner.forms import NewOwnerForm
from assign67.qdb.database_ import Database

owner_var = Blueprint('owner', __name__, template_folder='templates' , static_folder='static')

db = Database()

owners = []

#Function to refresh the global list of owneres by clearing and appending to the list
def refresh_owners():
    global owners
    owners.clear()
    try:
        for row in db.get_owners():
            owners.append(row)
    except:
        owners = ['Error retrieving from database']

refresh_owners()

@owner_var.route("/owners")
def list_owners():
    #Refreshes global owners list
    refresh_owners()

    return render_template('owners.html', owners=owners)

@owner_var.route("/register",  methods=['GET', 'POST'])
def register():
    #Creates new owner form object
    new_owner_form = NewOwnerForm()
    if new_owner_form.validate_on_submit():
        #Generates a hash for the password entered
        b = Bcrypt()
        hashed_password = b.generate_password_hash(new_owner_form.password.data).decode('utf-8')

        #Checks if the password confirmation passes
        if not new_owner_form.check_password():
            flash('Passwords must be the same', 'error')
        else:
            #First checks if this username already exist in db 
            if db.get_owner(new_owner_form.username.data) is not None:
                flash('This username is already taken', 'error')
            else:
                #Calls function to insert new owner into db
                try:        
                    db.add_new_owner(new_owner_form.username.data, new_owner_form.owner_name.data, new_owner_form.email.data, hashed_password, new_owner_form.occupation.data)
                    flash('New owner added', 'success')
                except:
                    flash('Error adding owner', 'error')

        return redirect(url_for('owner.register'))

    return render_template('register.html', form=new_owner_form)