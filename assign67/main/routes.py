from flask import Blueprint, render_template

main_var = Blueprint('main', __name__, template_folder="templates" , static_folder="static")

@main_var.route("/")
def home():
    return render_template('home.html')