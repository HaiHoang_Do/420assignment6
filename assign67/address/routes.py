from flask import Blueprint, render_template, url_for, redirect, flash

from assign67.address.address import Address
from assign67.address.forms import AddressForm
from assign67.note.note import Note
from assign67.qdb.database_ import Database

address_var = Blueprint('address', __name__, template_folder='templates' , static_folder='static')
db = Database()

addresses = []

#Function to refresh the global list of addresses by clearing and appending to the list
def refresh_addresses():
    global addresses
    addresses.clear()
    try:
        for row in db.get_addresses(True):
            addresses.append(Address(row[0], row[1], row[2], row[3], row[4]))
    except:
        addresses = ['Error retrieving from database']

refresh_addresses()

@address_var.route("/addressbook/<string:name>")
def address_book_search(name):
    #If the parameter 'name' is empty, redirects the user back to the addresses page
    if name == "":
        return redirect(url_for('address.address_book'))
    #Searches through list of existing addresses
    #If the parameter matches one of the addresses in the list, creates an Address object
    #The object id is used to retrieve the list of notes associated to this id
    for a in addresses:
        if a.name == name:
            user_address = Address(db.get_addresses(name)[0][0], db.get_addresses(name)[0][1], db.get_addresses(name)[0][2], db.get_addresses(name)[0][3], db.get_addresses(name)[0][4])

            user_notes = []
            for row in db.get_notes_user(name):
                user_notes.append(Note(row[0], row[2]))
            return render_template('specific_address.html', address=user_address, notes=user_notes)
    
    #Flashes error message if parameter does not match any name in the list of existing addresses
    flash("Address can not be found", 'error')
    return redirect(url_for('address.address_book'))

@address_var.route("/addressbook", methods=['GET', 'POST'])
def address_book():
    #Refreshed the list of existing addresses everytime this page is loaded
    refresh_addresses()

    #Creates form object to be used in this page
    address_form = AddressForm()
    if address_form.validate_on_submit():
        #Calls function to insert a new address into database using the form inputs
        #Flashes a status message depending on the result of running the query
        try:
            db.add_new_address(address_form.name.data, address_form.street.data, address_form.city.data, address_form.province.data)
            flash('Address added', 'success')
        except Exception as e:
            flash('Owner not registered', 'error')

        return redirect(url_for('address.address_book'))

    return render_template('address.html', addresses=addresses, form=address_form)