from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length


class AddressForm(FlaskForm):
    name = StringField('name', render_kw={"placeholder": "Enter name"}, validators=[DataRequired(), Length(min=1, max=50)])
    street = StringField('street', render_kw={"placeholder": "Enter street number"}, validators=[DataRequired(), Length(min=1, max=100)])
    city = StringField('city', render_kw={"placeholder": "Enter city"}, validators=[DataRequired(), Length(min=1, max=50)])
    province = StringField('province', render_kw={"placeholder": "Enter province"}, validators=[DataRequired(), Length(min=1, max=2)])

    submit = SubmitField('Add')