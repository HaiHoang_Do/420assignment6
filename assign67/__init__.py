from flask import Flask
from assign67.config import ConfigDev

def create_app():
    app = Flask(__name__)
    app.config.from_object(ConfigDev)
    app.config['SECRET_KEY'] = '1234'

    from assign67.main.routes import main_var
    from assign67.address.routes import address_var
    from assign67.owner.routes import owner_var

    app.register_blueprint(main_var)
    app.register_blueprint(address_var)
    app.register_blueprint(owner_var)

    return app